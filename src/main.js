import Vue from 'vue'
import VueMatomo from 'vue-matomo'
import VueRouter from 'vue-router'
import GetTextPlugin from 'vue-gettext'
import VueMeta from 'vue-meta'

import App from './App.vue'
import Home from './views/Home.vue'
import Help from './views/Help'
import Instances from './views/Instances'
import NotFound from './views/NotFound'
import AllContentSelections from './views/All-Content-Selections'

import './scss/main.scss'
import CommonMixins from './mixins/CommonMixins'

Vue.use(VueRouter)

// ############# I18N ##############

const availableLanguages = {
  'en_US': 'English',
  'fr_FR': 'Français',
  'de': 'Deutsch',
  'es': 'Español',
  'ru': 'русский',
  'sv': 'svenska'
}
const aliasesLanguages = {
  'en': 'en_US',
  'fr': 'fr_FR'
}
const allLocales = Object.keys(availableLanguages).concat(Object.keys(aliasesLanguages))

const defaultLanguage = 'en_US'
let currentLanguage = defaultLanguage

const localePath = window.location.pathname
  .replace(/^\//, '')
  .replace(/\/$/, '')

const languageFromLocalStorage = localStorage.getItem('language')

if (allLocales.includes(localePath)) {
  currentLanguage = aliasesLanguages[localePath] ? aliasesLanguages[localePath] : localePath
  localStorage.setItem('language', currentLanguage)
} else if (languageFromLocalStorage) {
  currentLanguage = languageFromLocalStorage
} else {
  const navigatorLanguage = window.navigator.userLanguage || window.navigator.language
  const snakeCaseLanguage = navigatorLanguage.replace('-', '_')
  currentLanguage = aliasesLanguages[snakeCaseLanguage] ? aliasesLanguages[snakeCaseLanguage] : snakeCaseLanguage
}

Vue.filter('translate', value => {
  return value ? Vue.prototype.$gettext(value.toString()) : ''
})

const p = buildTranslationsPromise(defaultLanguage, currentLanguage)

p.catch(err => {
  console.error('Cannot load translations.', err)
  return { default: {} }
}).then(translations => {
  Vue.use(GetTextPlugin, {
    translations,
    availableLanguages,
    defaultLanguage: 'en_US',
    silent: process.env.NODE_ENV === 'production'
  })

  Vue.config.language = currentLanguage

  // ###########################

  Vue.use(VueMeta)

  Vue.mixin(CommonMixins)

  const HallOfFame = () => import('./views/Hall-Of-Fame')
  const News = () => import('./views/News')
  const FAQ = () => import('./views/FAQ')

  const routes = [
    {
      path: '/',
      component: Home
    },
    {
      path: '/help',
      component: Help
    },
    {
      path: '/news',
      component: News
    },
    {
      path: '/instances',
      component: Instances
    },
    {
      path: '/hall-of-fame',
      component: HallOfFame
    },
    {
      path: '/faq',
      component: FAQ
    },
    {
      path: '/content-selections',
      component: AllContentSelections
    },
    {
      path: '/404',
      component: NotFound
    },
    {
      path: '*',
      redirect: '/404'
    }
  ]

  for (const locale of allLocales) {
    routes.push({
      path: '/' + locale,
      component: Home
    })
  }

  const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
    scrollBehavior (to, from, savedPosition) {
      if (to.hash) {
        return { selector: to.hash }
      } else {
        return { x: 0, y: 0 }
      }
    }
  })

  // Stats Matomo
  if (!(navigator.doNotTrack === 'yes' ||
    navigator.doNotTrack === '1' ||
    navigator.msDoNotTrack === '1' ||
    window.doNotTrack === '1')
  ) {
    Vue.use(VueMatomo, {
      // Configure your matomo server and site
      host: 'https://stats.framasoft.org/',
      siteId: 68,

      // Enables automatically registering pageviews on the router
      router,

      // Require consent before sending tracking information to matomo
      // Default: false
      requireConsent: false,

      // Whether to track the initial page view
      // Default: true
      trackInitialView: true,

      // Changes the default .js and .php endpoint's filename
      // Default: 'piwik'
      trackerFileName: 'p',

      enableLinkTracking: true
    })

    const _paq = _paq || [] // eslint-disable-line

    // CNIL conformity
    _paq.push([function piwikCNIL () {
      const self = this

      function getOriginalVisitorCookieTimeout () {
        const now = new Date()
        const nowTs = Math.round(now.getTime() / 1000)
        const visitorInfo = self.getVisitorInfo()
        const createTs = parseInt(visitorInfo[2], 10)
        const cookieTimeout = 33696000 // 13 months in seconds
        return (createTs + cookieTimeout) - nowTs
      }

      this.setVisitorCookieTimeout(getOriginalVisitorCookieTimeout())
    }])
  }

  new Vue({ // eslint-disable-line no-new
    el: '#app',
    router,
    mounted () {
      // You'll need this for renderAfterDocumentEvent.
      document.dispatchEvent(new Event('render-event'))
    },
    render: h => h(App)
  })
})

function buildTranslationsPromise (defaultLanguage, currentLanguage) {
  const translations = {}

  // No need to translate anything
  if (currentLanguage === defaultLanguage) return Promise.resolve(translations)

  // Fetch translations from server
  const fromRemote = import('./translations/' + currentLanguage + '.json')
    .then(module => {
      const remoteTranslations = module.default
      try {
        localStorage.setItem('translations-' + currentLanguage, JSON.stringify(remoteTranslations))
      } catch (err) {
        console.error('Cannot save translations in local storage.', err)
      }

      return Object.assign(translations, remoteTranslations)
    })

  // If we have a cache, try to
  const fromLocalStorage = localStorage.getItem('translations-' + currentLanguage)
  if (fromLocalStorage) {
    try {
      Object.assign(translations, JSON.parse(fromLocalStorage))

      return Promise.resolve(translations)
    } catch (err) {
      console.error('Cannot parse translations from local storage.', err)
    }
  }

  return fromRemote
}
